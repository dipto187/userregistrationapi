package com.example.registrationandlogin.application.service;

import com.example.registrationandlogin.adapter.out.persistence.entity.UserEntity;
import com.example.registrationandlogin.application.port.in.UserRegistrationUseCase;
import com.example.registrationandlogin.application.port.out.UserRegistrationPersistencePort;
import com.example.registrationandlogin.application.service.exception.DataNotFoundException;
import com.example.registrationandlogin.application.service.exception.DuplicateUserIdFoundException;
import com.example.registrationandlogin.application.service.exception.PasswordNotMatchedException;
import com.example.registrationandlogin.domain.IUser;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class UserService implements UserRegistrationUseCase {

    private UserRegistrationPersistencePort userRegistrationPersistencePort;

    public UserService(UserRegistrationPersistencePort userRegistrationPersistencePort){
        this.userRegistrationPersistencePort = userRegistrationPersistencePort;
    }

    @Override
    public boolean loginUser(UserEntity userEntity) {
        Optional<UserEntity> user = userRegistrationPersistencePort.findUser(userEntity);
        if(user.get().getPassword().equals(userEntity.getPassword())){
            return true;
        }
        else{
            throw new PasswordNotMatchedException("Password incorrect");
        }
    }

    public void registerUser(UserEntity userEntity){
        Optional<String> _userid = Optional.ofNullable(userEntity.getUserId());
        Optional<String> _password = Optional.ofNullable(userEntity.getPassword());
        Optional<String> _mobileno = Optional.ofNullable(userEntity.getMobileNo());

        if(_userid.isEmpty() || _userid.get().isBlank() || _userid.get().isEmpty()){
            throw new DataNotFoundException("userId not given");
        }
        if(_password.isEmpty() || _password.get().isBlank() || _password.get().isEmpty()){
            throw new DataNotFoundException("password not given");
        }
        if(_mobileno.isEmpty() || _mobileno.get().isBlank() || _mobileno.get().isEmpty()){
            throw new DataNotFoundException("mobileNo not given");
        }
        if(userRegistrationPersistencePort.checkDuplicate(userEntity.getUserId())){
            throw new DuplicateUserIdFoundException("UserId exists");
        }

        userRegistrationPersistencePort.saveUser(userEntity);

    }

    public void changePassword(UserEntity userEntity, String userId){
        userEntity.setUserId(userId);
        Optional<String> _password = Optional.ofNullable(userEntity.getPassword());
        if(_password.isEmpty() || _password.get().isBlank() || _password.get().isEmpty()){
            throw new DataNotFoundException("password not given");
        }
        userRegistrationPersistencePort.saveUser(userEntity);
    }

}
