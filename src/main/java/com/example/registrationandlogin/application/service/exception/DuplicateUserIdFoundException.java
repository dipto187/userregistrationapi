package com.example.registrationandlogin.application.service.exception;

public class DuplicateUserIdFoundException extends RuntimeException {
    public DuplicateUserIdFoundException(String message) {
        super(message);
    }
}
