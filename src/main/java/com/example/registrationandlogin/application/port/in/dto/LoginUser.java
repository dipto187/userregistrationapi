package com.example.registrationandlogin.application.port.in.dto;

public class LoginUser {
    private String userId;
    private String password;

    public LoginUser(String userId, String password) {
        this.userId = userId;
        this.password = password;
    }

    public LoginUser() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
