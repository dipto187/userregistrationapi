package com.example.registrationandlogin.application.port.in;

import com.example.registrationandlogin.adapter.out.persistence.entity.UserEntity;
import com.example.registrationandlogin.domain.IUser;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface UserRegistrationUseCase {
    public boolean loginUser(UserEntity userEntity);
    public void registerUser(UserEntity userEntity);
    public void changePassword(UserEntity userEntity, String id);
}
