package com.example.registrationandlogin.application.port.out;

import com.example.registrationandlogin.adapter.out.persistence.entity.UserEntity;

import java.util.Optional;

public interface UserRegistrationPersistencePort {
    public String saveUser(UserEntity userEntity);
    public String updateUser(UserEntity userEntity);

    public boolean checkDuplicate(String id);

    public Optional<UserEntity> findUser(UserEntity userEntity);


}
