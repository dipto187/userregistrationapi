package com.example.registrationandlogin.adapter.out.persistence.entity;

import com.sun.istack.NotNull;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserEntity {
    @Id
    @NotNull
    private String userId;
    private String password;
    private String mobileNo;

    public UserEntity(String userId, String password, String mobileNo) {
        this.userId = userId;
        this.password = password;
        this.mobileNo = mobileNo;
    }

    public UserEntity() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
