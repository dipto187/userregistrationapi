package com.example.registrationandlogin.adapter.out.persistence;

import com.example.registrationandlogin.adapter.out.persistence.entity.UserEntity;
import com.example.registrationandlogin.adapter.out.persistence.repository.UserRepository;
import com.example.registrationandlogin.application.port.out.UserRegistrationPersistencePort;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserRegistrationPersistenceAdapter implements UserRegistrationPersistencePort {

    private final UserRepository userRepository;

    public UserRegistrationPersistenceAdapter(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public String saveUser(UserEntity userEntity) {
        return userRepository.save(userEntity).toString();
    }

    @Override
    public String updateUser(UserEntity userEntity) {
        return userRepository.save(userEntity).toString();
    }

    @Override
    public boolean checkDuplicate(String id){
        return userRepository.existsById(id);
    }

    @Override
    public Optional<UserEntity> findUser(UserEntity userEntity) {
        return userRepository.findById(userEntity.getUserId());
    }

}
