package com.example.registrationandlogin.adapter.out.persistence.repository;

import com.example.registrationandlogin.adapter.out.persistence.entity.UserEntity;
import com.example.registrationandlogin.domain.IUser;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity,String> {

}
