package com.example.registrationandlogin.adapter.in.web.controllers;

import com.example.registrationandlogin.adapter.out.persistence.entity.UserEntity;
import com.example.registrationandlogin.application.port.in.UserRegistrationUseCase;
import com.example.registrationandlogin.application.service.exception.DataNotFoundException;
import com.example.registrationandlogin.application.service.exception.DuplicateUserIdFoundException;
import com.example.registrationandlogin.application.service.exception.PasswordNotMatchedException;
import com.example.registrationandlogin.domain.IUser;
import com.example.registrationandlogin.application.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserRegistrationController {

    UserRegistrationUseCase userRegistrationUseCase;

    public UserRegistrationController(UserRegistrationUseCase userRegistrationUseCase) {
        this.userRegistrationUseCase = userRegistrationUseCase;
    }

    @PostMapping("/login")
    public ResponseEntity<String> getAllUser(@RequestBody UserEntity userEntity){
        try{
            if(userRegistrationUseCase.loginUser(userEntity)){
                return ResponseEntity.ok("login successfull");
            }
        }
        catch (Exception e){
            if(e instanceof PasswordNotMatchedException){
                ResponseEntity.badRequest().build();
            }
        }
        return ResponseEntity.badRequest().build();
    }


    @PostMapping("/register")
    public void registerUser(@RequestBody UserEntity user){
        try{
            userRegistrationUseCase.registerUser(user);
        }
        catch(Exception e){
            if(e instanceof DataNotFoundException){
                ResponseEntity.notFound().build();
            }
            if(e instanceof DuplicateUserIdFoundException){
                ResponseEntity.badRequest().build();
            }
        }
    }

    @PutMapping("/forgetpass/{userid}")
    public void changePassword(@RequestBody UserEntity user, @PathVariable String userid){
        try{
            userRegistrationUseCase.changePassword(user,userid);
        }
        catch (Exception e){
            if(e instanceof DataNotFoundException){
                ResponseEntity.badRequest().build();
            }
        }
    }
}
