package com.example.registrationandlogin.domain;

public class IUser {
    private String userId;
    private String password;
    private String mobileNo;

    public IUser(String userId, String password, String mobileNo) {
        this.userId = userId;
        this.password = password;
        this.mobileNo = mobileNo;
    }

    public IUser() {

    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
